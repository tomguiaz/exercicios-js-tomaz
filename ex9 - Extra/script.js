var notas = [91, 54, 67, 79, 81, 100];

var str = "";
notas.forEach((e) => str+= (' ' + e));
console.log(str);

str = "";
var notasLetras = notas.map((function(nota){
    if(nota >= 90){
        return 'A';
    }else if(nota >=80 && nota <90){
        return 'B';
    }else if(nota>=70 && nota<80){
        return 'C';
    }else if(nota>=60 && nota<70){
        return 'D';
    }else{
        return 'F';
    }
}));
notasLetras.forEach((e) => str+= ('  ' + e));
console.log(str);