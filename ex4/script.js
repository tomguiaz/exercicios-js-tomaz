//Q1
console.log("***********Q1***********")
console.log(gods.forEach((g)=>{console.log(g.name + " "+g.features.length)}));



//Q2
var midGods = gods.filter(g => g.roles.includes('Mid'));
console.log("**********************");
console.log("*********** Q2 MID GODS***********");
console.log("**********************");
midGods.forEach(g => console.log(g));

//Q3
console.log("**********************");
console.log("*********** Q3 SORTED GODS***********");
console.log("**********************");
gods.sort((g, g2)=> g.pantheon.localeCompare(g2.pantheon));
gods.forEach(g => console.log(g));

//Q4
console.log("**********************");
console.log("*********** Q4 NAME + CLASS ***********");
console.log("**********************");
var godsList = gods.map((g)=>{
   return g.name +" ("+g.class+")"
});

godsList.forEach(x => console.log(x));
