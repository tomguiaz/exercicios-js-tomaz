const balance = {
   receitas:[100,200,300,500],
   despesas: [45, 69.5, 250.4, 320.4]
}

function calcExpenses(){
   let sum = 0.0;
   balance.despesas.forEach((x)=> sum += x);
   return sum;
}

function calcRevenue(){
   let sum = 0.0;
   balance.receitas.forEach((x)=> sum+=x);
   return sum;
}
function balanceResult(){
   let expenses= calcExpenses();
   let revenue = calcRevenue();

   let balanceResult = revenue - expenses;
   if(balanceResult > 0){
      alert("Parabéns! =) Seu saldo está positivo! Saldo: "+ balanceResult);
   }else if(balanceResult <0){
      alert("Seu saldo está NEGATIVO! Saldo: "+ balanceResult);
   }else{
      alert('Zerado =( Melhor do que parar no serasa');
   }
}

balanceResult();