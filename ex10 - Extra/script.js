console.log("Numero de categorias: "+ booksByCategory.length);

//conta numero de livros por categoria
booksByCategory.forEach((obj)=>console.log("Categoria: " + obj.category + ": " + obj.books.length +" livros"))

//conta numero de autores por categoria

//cria array de autores
let authors = [];
booksByCategory.forEach((obj)=>{
   //obj = categoria obj.books = lista de livros da categoria
   obj.books.forEach( (el)=>{
         if(!authors.includes(el.author)){
            console.log(el.author);
            authorVectorSplit = el.author.split(" e ");
          
            if(authorVectorSplit.length > 1){
               //Significa que é uma coautoria, devemos checar se os autores ja foram listados
               console.log("ohayo");
               authorVectorSplit.forEach((str)=>{
                  if(!authors.includes(str)){
                     authors.push(str);
                  }
               })
            }else{
               authorVectorSplit.forEach((str)=>{
                  authors.push(str);
               })
               
               }   
            }
      }
   );
});

function showAugustoCury(){
   console.log("***************** LIVROS DO AUGUSTO CURY ******************");
   booksByCategory.forEach((ctg)=>{
      let ctg_books = ctg.books;
      ctg_books.forEach((book)=>{
         let authorSplit = book.author.split(" e ");
         if(authorSplit.includes('Augusto Cury')){
            console.log(book.title);
         }
      });
   });
}

function showBookByAuthor(name){
   console.log("**********BUSCA POR LIVROS DE " +name+ "**********");
   booksByCategory.forEach((ctg)=>{
      let ctg_books = ctg.books;
      ctg_books.forEach((book)=>{
         let authorSplit = book.author.split(" e ");
         if(authorSplit.includes(name)){
            console.log(book.title);
         }
      });
   });
}
showAugustoCury();

showBookByAuthor('George S. Clason');

showBookByAuthor('Isaac Asimov');
